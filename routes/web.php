<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\WebsiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', [HomeController::class, 'index'])->name('home');

Route::resource('websites', WebsiteController::class);
Route::delete('websites/force/{website}', [WebsiteController::class, 'forceDestroy'])->name('websites.force.destroy');
Route::put('websites/restore/{website}', [WebsiteController::class, 'restore'])->name('websites.restore');

Route::get('customer/{slug}/websites', [WebsiteController::class, 'index'])->name('websites.customer');
