#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: client
#------------------------------------------------------------

CREATE TABLE client(
        id_client      Int  Auto_increment  NOT NULL ,
        nom_client     Varchar (50) NOT NULL ,
        tel_client     Int NOT NULL ,
        email_client   Varchar (50) NOT NULL ,
        adresse_client Varchar (50)
	,CONSTRAINT client_PK PRIMARY KEY (id_client)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: entreprise
#------------------------------------------------------------

CREATE TABLE entreprise(
        id_ent      Int  Auto_increment  NOT NULL ,
        nom_ent     Varchar (50) NOT NULL ,
        adresse_ent Varchar (200)
	,CONSTRAINT entreprise_PK PRIMARY KEY (id_ent)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: techno
#------------------------------------------------------------

CREATE TABLE techno(
        id_techno  Int  Auto_increment  NOT NULL ,
        nom_techno Varchar (50) NOT NULL
	,CONSTRAINT techno_PK PRIMARY KEY (id_techno)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: service
#------------------------------------------------------------

CREATE TABLE service(
        id_service  Int  Auto_increment  NOT NULL ,
        nom_service Varchar (20) NOT NULL
	,CONSTRAINT service_PK PRIMARY KEY (id_service)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: serveur
#------------------------------------------------------------

CREATE TABLE serveur(
        id_serveur  Int  Auto_increment  NOT NULL ,
        nom_serveur Varchar (50) NOT NULL
	,CONSTRAINT serveur_PK PRIMARY KEY (id_serveur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: site
#------------------------------------------------------------

CREATE TABLE site(
        id_site          Int  Auto_increment  NOT NULL ,
        nom_site         Varchar (50) NOT NULL ,
        url_site         Varchar (50) NOT NULL ,
        url_admin_site   Varchar (100) NOT NULL ,
        codes_acces_site Varchar (100) NOT NULL ,
        uniweb_site      Bool NOT NULL ,
        id_serveur       Int NOT NULL
	,CONSTRAINT site_PK PRIMARY KEY (id_site)

	,CONSTRAINT site_serveur_FK FOREIGN KEY (id_serveur) REFERENCES serveur(id_serveur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: utiliser
#------------------------------------------------------------

CREATE TABLE utiliser(
        id_techno Int NOT NULL ,
        id_site   Int NOT NULL
	,CONSTRAINT utiliser_PK PRIMARY KEY (id_techno,id_site)

	,CONSTRAINT utiliser_techno_FK FOREIGN KEY (id_techno) REFERENCES techno(id_techno)
	,CONSTRAINT utiliser_site0_FK FOREIGN KEY (id_site) REFERENCES site(id_site)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: travailler
#------------------------------------------------------------

CREATE TABLE travailler(
        id_ent    Int NOT NULL ,
        id_client Int NOT NULL
	,CONSTRAINT travailler_PK PRIMARY KEY (id_ent,id_client)

	,CONSTRAINT travailler_entreprise_FK FOREIGN KEY (id_ent) REFERENCES entreprise(id_ent)
	,CONSTRAINT travailler_client0_FK FOREIGN KEY (id_client) REFERENCES client(id_client)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: proposer
#------------------------------------------------------------

CREATE TABLE proposer(
        id_service Int NOT NULL ,
        id_site    Int NOT NULL
	,CONSTRAINT proposer_PK PRIMARY KEY (id_service,id_site)

	,CONSTRAINT proposer_service_FK FOREIGN KEY (id_service) REFERENCES service(id_service)
	,CONSTRAINT proposer_site0_FK FOREIGN KEY (id_site) REFERENCES site(id_site)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: appartenir
#------------------------------------------------------------

CREATE TABLE appartenir(
        id_site   Int NOT NULL ,
        id_client Int NOT NULL
	,CONSTRAINT appartenir_PK PRIMARY KEY (id_site,id_client)

	,CONSTRAINT appartenir_site_FK FOREIGN KEY (id_site) REFERENCES site(id_site)
	,CONSTRAINT appartenir_client0_FK FOREIGN KEY (id_client) REFERENCES client(id_client)
)ENGINE=InnoDB;

