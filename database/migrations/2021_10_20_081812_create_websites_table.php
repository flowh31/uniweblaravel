<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('websites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->binary('logo')->nullable();
            $table->boolean('madein')->nullable();
            $table->string('url_dev')->nullable();
            $table->string('url_dev_admin')->nullable();
            $table->string('access_dev')->nullable();
            $table->string('url_prod')->nullable();
            $table->string('url_prod_admin')->nullable();
            $table->string('access_prod')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('customer_id');
            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('websites');
    }
}
