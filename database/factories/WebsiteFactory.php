<?php

namespace Database\Factories;

use App\Models\Website;
use Illuminate\Database\Eloquent\Factories\Factory;

class WebsiteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Website::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        'name' => $this->faker->sentence(2, true),
        'logo' => $this->faker->imageUrl($width = 640, $height = 480),
        'madein' => $this->faker->boolean(),
        'url_dev'=> $this->faker->url(),
        'url_dev_admin' => $this->faker->url(),
        'access_dev' => $this->faker->slug(),
        'url_prod' => $this->faker->url(),
        'url_prod_admin' => $this->faker->url(),
        'access_prod' => $this->faker->slug(),
        ];
    }
}
