<?php

namespace Database\Seeders;

use App\Models\Website;
use App\Models\Customer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\Website::factory(10)->create();
        Customer::factory(20)->create()->each(function ($customer) {
            $i = rand(1, 4);
            while (--$i) {
                $customer->websites()->save(Website::factory()->make());
            }
        });
    }
}
