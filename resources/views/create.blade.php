@extends('layouts.master')
@section('content')
    <div class="card">
        <header class="card-header">
            <p class="card-header-title">Création d'un site</p>
        </header>
        <div class="card-content">
            <div class="content">
                <form action="{{ route('websites.store') }}" method="POST">
                    @csrf
                    <div class="field">
                        <label class="label">Client</label>
                        <div class="select">
                            <select name="customer_id">
                                @foreach ($customers as $customer)
                                    <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Nom du site</label>
                        <div class="control">
                            <input class="input @error('name') is-danger @enderror" type="text" name="name"
                                value="{{ old('name') }}" placeholder="Nom du site">
                        </div>
                        @error('name')
                            <p class="help is-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    {{-- <div class="field">
                        <label class="label">Année de diffusion</label>
                        <div class="control">
                            <input class="input" type="number" name="year" value="{{ old('year') }}" min="1950"
                                max="{{ date('Y') }}">
                        </div>
                        @error('year')
                            <p class="help is-danger">{{ $message }}</p>
                        @enderror
                    </div> --}}

                    <div class="field">
                        <label class="label">Made In Uniweb ?</label>
                        <div class="field">
                            <input type="radio" id="yes" name="madein" value="1">
                            <label for="yes">Oui</label>
                        </div>
                        <div>
                            <input type="radio" id="no" name="madein" value="0" checked>
                            <label for="no">Non</label>
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">URL DEV</label>
                        <div class="control">
                            <input class="input @error('url_dev') is-danger @enderror" type="text" name="url_dev"
                                value="{{ old('url_dev') }}" placeholder="URL du site en dev">
                        </div>
                        @error('url_dev')
                            <p class="help is-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="field">
                        <label class="label">URL DEV ADMIN</label>
                        <div class="control">
                            <input class="input @error('url_dev_admin') is-danger @enderror" type="text"
                                name="url_dev_admin" value="{{ old('url_dev_admin') }}"
                                placeholder="URL admin du site en dev">
                        </div>
                        @error('url_dev_admin')
                            <p class="help is-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="field">
                        <label class="label">ACCES SITE EN DEV</label>
                        <div class="control">
                            <input class="input @error('access_dev') is-danger @enderror" type="text" name="access_dev"
                                value="{{ old('access_dev') }}" placeholder="Accès du site en dev">
                        </div>
                        @error('access_dev')
                            <p class="help is-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="field">
                        <label class="label">URL PROD</label>
                        <div class="control">
                            <input class="input @error('url_prod') is-danger @enderror" type="text" name="url_prod"
                                value="{{ old('url_prod') }}" placeholder="URL du site en prod">
                        </div>
                        @error('url_prod')
                            <p class="help is-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="field">
                        <label class="label">URL PROD ADMIN</label>
                        <div class="control">
                            <input class="input @error('url_prod_admin') is-danger @enderror" type="text"
                                name="url_prod_admin" value="{{ old('url_prod_admin') }}"
                                placeholder="URL admin du site en prod">
                        </div>
                        @error('url_prod_admin')
                            <p class="help is-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="field">
                        <label class="label">ACCES SITE EN PROD</label>
                        <div class="control">
                            <input class="input @error('access_prod') is-danger @enderror" type="text" name="access_prod"
                                value="{{ old('access_prod') }}" placeholder="Accès du site en prod">
                        </div>
                        @error('access_prod')
                            <p class="help is-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="field">
                        <div class="control">
                            <button class="button is-link">Créer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
