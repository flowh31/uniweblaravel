@extends('layouts.master')
@section('css')
    <style>
        .card-footer {
            justify-content: center;
            align-items: center;
            padding: 0.4em;
        }

        select,
        .is-info {
            margin: 0.3em;
        }

    </style>
@endsection
@section('content')
    @if (session()->has('info'))
        <div class="notification is-success">
            {{ session('info') }}
        </div>
    @endif
    <div class="card">
        <header class="card-header">
            <p class="card-header-title">Websites</p>
            <div class="select">
                <select onchange="window.location.href = this.value">
                    <option value="{{ route('websites.index') }}" @unless($slug) selected @endunless>Trier par client
                    </option>
                    @foreach ($customers as $customer)
                        <option value="{{ route('websites.customer', $customer->slug) }}"
                            {{ $slug == $customer->slug ? 'selected' : '' }}>{{ $customer->name }}</option>
                    @endforeach
                </select>
            </div>
            <a class="button is-info" href="{{ route('websites.create') }}">Ajouter un site</a>
        </header>
        <div class="card-content">
            <div class="content">
                <table class="table is-hoverable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nom du site</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($websites as $website)
                            <tr @if ($website->deleted_at) class="has-background-grey-lighter" @endif>
                                <td>{{ $website->id }}</td>
                                <td><b>{{ $website->name }}</b></td>
                                <td>
                                    @if ($website->deleted_at)
                                        <form action="{{ route('websites.restore', $website->id) }}" method="post">
                                            @csrf
                                            @method('PUT')
                                            <button class="button is-primary" type="submit">Restaurer</button>
                                        </form>
                                    @else
                                        <a class="button is-primary"
                                            href="{{ route('websites.show', $website->id) }}">Voir</a>
                                    @endif
                                </td>
                                <td>
                                    @if ($website->deleted_at)
                                    @else
                                        <a class="button is-warning"
                                            href="{{ route('websites.edit', $website->id) }}">Modifier</a>
                                    @endif
                                </td>
                                <td>
                                    <form
                                        action="{{ route($website->deleted_at ? 'websites.force.destroy' : 'websites.destroy', $website->id) }}"
                                        method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="button is-danger" type="submit">Supprimer</button>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <footer class="card-footer is-centered">
            {{ $websites->links() }}
        </footer>
    </div>
@endsection
