@extends('layouts.master')
@section('css')
    <style>
        .card-footer {
            justify-content: center;
            align-items: center;
            padding: 0.4em;
        }

        select,
        .is-info {
            margin: 0.3em;
        }

    </style>
@endsection
@section('content')
    <div class="card">
        <header class="card-header">
            <p class="card-header-title">Nom du site : {{ $website->name }}</p>
            <p class="card-header-title">Client : {{ $customer }}</p>
            <img src="{{ $website->logo }}" style="width: 100px; height:100px; object-fit:cover">
        </header>
        <div class="card-content">
            <div class="content">
                <p><b>Made In UNIWEB ? :</b> {{ $website->logo == 1 ? 'Oui' : 'Non' }}</p>
                <p><b>URL DEV :</b> {{ $website->url_dev }}</p>
                <p><b>URL DEV ADMIN :</b> {{ $website->url_dev_admin }}</p>
                <p><b>ACCESS DEV :</b> {{ $website->access_dev }}</p>
                <p><b>URL PROD :</b> {{ $website->url_prod }}</p>
                <p><b>URL PROD ADMIN :</b> {{ $website->url_prod_admin }}</p>
                <p><b>ACCESS PROD :</b> {{ $website->access_prod }}</p>
            </div>
        </div>
    </div>
@endsection
