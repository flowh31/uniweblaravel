<?php

namespace App\Http\Controllers;

use App\Models\{Website, Customer};
use Illuminate\Http\Request;
use App\Http\Requests\Website as WebsiteRequest;


class WebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = null)
    {
        // $websites = Website::withTrashed()->oldest('name')->paginate(20);
        // return view('index', compact('websites'));

        // On distingue le cas où on fournit un slug de catégorie et le cas où on n'en fournit pas
        $query = $slug ? Customer::whereSlug($slug)->firstOrFail()->websites() : Website::query();
        $websites = $query->withTrashed()->oldest('name')->paginate(50);
        $customers = Customer::all();
        return view('index', compact('websites', 'customers', 'slug'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('create');
        $customers = Customer::all();
        return view('create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebsiteRequest $websiteRequest)
    {
        Website::create($websiteRequest->all());
        return redirect()->route('websites.index')->with('info', 'Le site a bien été créé');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Website $website)
    {
        // return view('show', compact('website'));
        $customer = $website->customer->name;
        return view('show', compact('website', 'customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Website $website)
    {
        return view('edit', compact('website'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WebsiteRequest $websiteRequest, Website $website)
    {
        $website->update($websiteRequest->all());
        return redirect()->route('websites.index')->with('info', 'Le site a bien été modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Website $website)
    {
        $website->delete();
        return back()->with('info', 'Le site a bien été mis dans la corbeille.');
    }

    public function forceDestroy($id)
    {
        Website::withTrashed()->whereId($id)->firstOrFail()->forceDelete();
        return back()->with('info', 'Le site a bien été supprimé définitivement dans la base de données.');
    }

    public function restore($id)
    {
        Website::withTrashed()->whereId($id)->firstOrFail()->restore();
        return back()->with('info', 'Le site a bien été restauré.');
    }
}
