<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Website extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'logo' => 'nullable|string|max:100',
            'madein' => 'nullable|boolean',
            'url_dev' => 'nullable|string|max:100',
            'url_dev_admin' => 'nullable|string|max:100',
            'access_dev' => 'nullable|string|max:100',
            'url_prod' => 'nullable|string|max:100',
            'url_prod_admin' => 'nullable|string|max:100',
            'access_prod' => 'nullable|string|max:100'
        ];
    }
}
